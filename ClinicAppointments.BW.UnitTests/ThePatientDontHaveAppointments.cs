﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ClinicAppointments.BW.UnitTests
{
    [TestClass]
    public class ThePatientDontHaveAppointments
    {
        [TestMethod]
        public void ThePatientDontHaveAppointments_DayNotListed_Yes()
        {
            bool expectedValue = true;
            bool obtainedValue;

            IList<DateTime> list = new List<DateTime>();
            DateTime today = DateTime.Now;
            list.Add(today.AddDays(1));
            list.Add(today.AddDays(-1));

            obtainedValue = BusinessWorkFlow.ThePatientDontHaveAppointments(today, list);

            Assert.AreEqual(expectedValue, obtainedValue);
        }

        [TestMethod]
        public void ThePatientDontHaveAppointments_DayListed_No()
        {
            bool expectedValue = true;
            bool obtainedValue;

            IList<DateTime> list = new List<DateTime>();
            DateTime today = DateTime.Now;
            list.Add(today);
            list.Add(today.AddDays(-1));

            obtainedValue = BusinessWorkFlow.ThePatientDontHaveAppointments(today, list);

            Assert.AreNotEqual(expectedValue, obtainedValue);
        }
    }
}
