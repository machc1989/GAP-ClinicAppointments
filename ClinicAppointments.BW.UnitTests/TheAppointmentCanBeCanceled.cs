﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ClinicAppointments.BW.UnitTests
{
    [TestClass]
    public class TheAppointmentCanBeCanceled
    {
        [TestMethod]
        public void TheAppointmentCanBeCanceled_DateOutOfRange_Yes()
        {
            bool expectedValue = true;
            bool obtainedValue;
            DateTime today = DateTime.Now;
            DateTime dateOfOutRange = today.AddDays(2);
            obtainedValue = BusinessWorkFlow.TheAppointmentCanBeCanceled(dateOfOutRange);

            Assert.AreEqual(expectedValue, obtainedValue);
        }

        [TestMethod]
        public void TheAppointmentCanBeCanceled_DateInLimit_Yes()
        {
            bool expectedValue = true;
            bool obtainedValue;
            DateTime today = DateTime.Now;
            DateTime dateOfOutRange = today.AddDays(1);
            obtainedValue = BusinessWorkFlow.TheAppointmentCanBeCanceled(dateOfOutRange);

            Assert.AreEqual(expectedValue, obtainedValue);
        }

        [TestMethod]
        public void TheAppointmentCanBeCanceled_DateNow_No()
        {
            bool expectedValue = true;
            bool obtainedValue;
            DateTime today = DateTime.Now;
            obtainedValue = BusinessWorkFlow.TheAppointmentCanBeCanceled(today);

            Assert.AreNotEqual(expectedValue, obtainedValue);
        }

        [TestMethod]
        public void TheAppointmentCanBeCanceled_DateInThePast_No()
        {
            bool expectedValue = true;
            bool obtainedValue;
            DateTime today = DateTime.Now;
            DateTime dateInThePast = today.AddDays(-1);
            obtainedValue = BusinessWorkFlow.TheAppointmentCanBeCanceled(dateInThePast);

            Assert.AreNotEqual(expectedValue, obtainedValue);
        }
    }
}
