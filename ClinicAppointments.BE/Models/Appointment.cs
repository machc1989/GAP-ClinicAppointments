﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ClinicAppointments.BE.Models
{
    public class Appointment
    {
        [Key]
        public int AppointmentId { get; set; }
        public int PatientId { get; set; }
        public int SpecialityId { get; set; }
        public DateTime AppointmentDate { get; set; }

        public virtual Patient Patient { get; set; }
        public virtual Speciality Speciality { get; set; }
    }
}