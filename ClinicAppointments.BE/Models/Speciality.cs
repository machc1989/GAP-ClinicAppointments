﻿using System.ComponentModel.DataAnnotations;

namespace ClinicAppointments.BE.Models
{
    public class Speciality
    {
        [Key]
        public int SpecialityId { get; set; }
        public string Name { get; set; }
    }
}