namespace ClinicAppointments.BE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Keys : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Appointments", newName: "AppointmentModels");
            RenameTable(name: "dbo.Patients", newName: "PatientModels");
            RenameTable(name: "dbo.Specialities", newName: "SpecialityModels");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.SpecialityModels", newName: "Specialities");
            RenameTable(name: "dbo.PatientModels", newName: "Patients");
            RenameTable(name: "dbo.AppointmentModels", newName: "Appointments");
        }
    }
}
