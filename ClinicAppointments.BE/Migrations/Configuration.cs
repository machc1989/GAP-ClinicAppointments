namespace ClinicAppointments.BE.Migrations
{
    using ClinicAppointments.BE.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ClinicAppointments.BE.DAL.ClinicContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "ClinicAppointments.BE.DAL.ClinicContext";
        }

        protected override void Seed(ClinicAppointments.BE.DAL.ClinicContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            context.Specialities.AddOrUpdate(x => x.SpecialityId,
                new Speciality() { SpecialityId = 1, Name = "Medicina General" },
                new Speciality() { SpecialityId = 2, Name = "Odontolog�a"},
                new Speciality() { SpecialityId = 3, Name = "Pediatr�a"},
                new Speciality() { SpecialityId = 4, Name = "Neurolog�a"}
                );

            context.Patients.AddOrUpdate(x => x.PatientId,
                new Patient() { PatientId = 1, Name = "Carlos", LastName = "Alvarado", Address = "Casa Presidencial", Email = "presidencia@gobierno.go.cr", Telephone = "2222-5555" },
                new Patient() { PatientId = 2, Name = "Manuel", LastName = "Bermudez", Address = "San Jos�, Central, Merced", Email = "mbermudez@mail.com", Telephone = "2568-9874" },
                new Patient() { PatientId = 3, Name = "Ramiro", LastName = "Castro", Address = "Heredia, Barva, Barva", Email = "rcastro@mail.com", Telephone = "8855-7744" },
                new Patient() { PatientId = 4, Name = "Kassandra", LastName = "Delgado", Address = "Alajuela, Alfaro Ruiz, Zarcero", Email = "kdelgado@mail.com", Telephone = "2314-5898" },
                new Patient() { PatientId = 5, Name = "Luisa", LastName = "Espinoza", Address = "Guanacaste, Carillo, Sardinal", Email = "lespinoza@mail.com", Telephone = "2269-8574" },
                new Patient() { PatientId = 6, Name = "Elisa", LastName = "Fallas", Address = "Puntarenas, Aguirre, Naranjito", Email = "efallas@mail.com", Telephone = "8369-5744" },
                new Patient() { PatientId = 7, Name = "Hernan", LastName = "Garibaldi", Address = "Cartago, Alvarado, Cervantes", Email = "hgaribaldi@mail.com", Telephone = "6001-8865" },
                new Patient() { PatientId = 8, Name = "Melissa", LastName = "Herrera", Address = "Heredia, San Rafael, Santiago", Email = "mherrera@mail.com", Telephone = "7002-2258" },
                new Patient() { PatientId = 9, Name = "Roberto", LastName = "Jimenez", Address = "Alajuela, San Mateo, Desmonte", Email = "rjimenez@mail.com", Telephone = "8596-7455" },
                new Patient() { PatientId = 10, Name = "Yamileth", LastName = "Hidalgo", Address = "Limon, Matina, Battan", Email = "yhidalgo@mail.com", Telephone = "6698-4214" },
                new Patient() { PatientId = 11, Name = "Rodrigo", LastName = "Quiros", Address = "Puntarenas, Corredores, Canoas", Email = "rquiros@mail.com", Telephone = "8855-4477" },
                new Patient() { PatientId = 12, Name = "Maria", LastName = "Brice�o", Address = "Cartago, Turrialba, Tuis", Email = "mbriceno@mail.com", Telephone = "8765-4321" },
                new Patient() { PatientId = 13, Name = "Esteban", LastName = "Zeledon", Address = "Heredia, Sarapiqu�, Horquetas", Email = "ezeledon@mail.com", Telephone = "8123-4567" },
                new Patient() { PatientId = 14, Name = "Carolina", LastName = "Mu�oz", Address = "Guanacaste, Abangares, Colorado", Email = "cmunoz@mail.com", Telephone = "8596-6958" }
                );

            int currentYear = DateTime.Now.Year;
            int currentMonth = DateTime.Now.Month;
            int currentDay = DateTime.Now.Day;

            context.Appointments.AddOrUpdate(x => x.AppointmentId,
                new Appointment() { AppointmentId = 1, PatientId = 1, SpecialityId = 1, AppointmentDate = new DateTime(currentYear, currentMonth, currentDay, 8, 30, 0) },
                new Appointment() { AppointmentId = 2, PatientId = 3, SpecialityId = 1, AppointmentDate = new DateTime(currentYear, currentMonth, currentDay, 10, 30, 0).AddDays(1) },
                new Appointment() { AppointmentId = 3, PatientId = 5, SpecialityId = 2, AppointmentDate = new DateTime(currentYear, currentMonth, currentDay, 14, 30, 0).AddDays(2) },
                new Appointment() { AppointmentId = 4, PatientId = 7, SpecialityId = 3, AppointmentDate = new DateTime(currentYear, currentMonth, currentDay, 16, 30, 0) },
                new Appointment() { AppointmentId = 5, PatientId = 9, SpecialityId = 1, AppointmentDate = new DateTime(currentYear, currentMonth, currentDay, 9, 30, 0).AddDays(1) },
                new Appointment() { AppointmentId = 6, PatientId = 11, SpecialityId = 4, AppointmentDate = new DateTime(currentYear, currentMonth, currentDay, 11, 30, 0).AddDays(2) },
                new Appointment() { AppointmentId = 7, PatientId = 13, SpecialityId = 1, AppointmentDate = new DateTime(currentYear, currentMonth, currentDay, 13, 30, 0) },
                new Appointment() { AppointmentId = 8, PatientId = 2, SpecialityId = 1, AppointmentDate = new DateTime(currentYear, currentMonth, currentDay, 15, 30, 0).AddDays(1) },
                new Appointment() { AppointmentId = 9, PatientId = 4, SpecialityId = 2, AppointmentDate = new DateTime(currentYear, currentMonth, currentDay, 8, 0, 0).AddDays(2) },
                new Appointment() { AppointmentId = 10, PatientId = 6, SpecialityId = 3, AppointmentDate = new DateTime(currentYear, currentMonth, currentDay, 10, 0, 0) },
                new Appointment() { AppointmentId = 11, PatientId = 8, SpecialityId = 1, AppointmentDate = new DateTime(currentYear, currentMonth, currentDay, 14, 0, 0).AddDays(1) },
                new Appointment() { AppointmentId = 12, PatientId = 10, SpecialityId = 4, AppointmentDate = new DateTime(currentYear, currentMonth, currentDay, 16, 0, 0).AddDays(2) }
                );
        }
    }
}
