namespace ClinicAppointments.BE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangModesName : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.AppointmentModels", newName: "Appointments");
            RenameTable(name: "dbo.PatientModels", newName: "Patients");
            RenameTable(name: "dbo.SpecialityModels", newName: "Specialities");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Specialities", newName: "SpecialityModels");
            RenameTable(name: "dbo.Patients", newName: "PatientModels");
            RenameTable(name: "dbo.Appointments", newName: "AppointmentModels");
        }
    }
}
