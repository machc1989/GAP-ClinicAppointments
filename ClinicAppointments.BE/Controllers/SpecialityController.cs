﻿using ClinicAppointments.BE.DAL;
using ClinicAppointments.BE.DAL.Interfaces;
using ClinicAppointments.BE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ClinicAppointments.BE.Controllers
{
    public class SpecialityController : ApiController
    {
        ISpeciality speciality;

        public SpecialityController()
        {
            speciality = new SpecialityRepository();
        }

        [HttpGet]
        public IEnumerable<Speciality> GetSpecialities()
        {
            return speciality.GetSpecialities();
        }

        [HttpGet]
        public Speciality GetSpeciality(int specialityId)
        {
            return speciality.GetSpecialityById(specialityId);
        }
    }
}
