﻿using ClinicAppointments.BE.DAL;
using ClinicAppointments.BE.DAL.Interfaces;
using ClinicAppointments.BE.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ClinicAppointments.BE.Controllers
{
    public class PatientController : ApiController
    {
        IPatient patientRepository;

        public PatientController()
        {
            patientRepository = new PatientRepository();
        }

        [HttpPost]
        public HttpResponseMessage AddPatientInformation([FromBody]Patient patient)
        {
            HttpResponseMessage response;
            try
            {
                patientRepository.InsertPatient(patient);
                patientRepository.Save();
                response = Request.CreateResponse(HttpStatusCode.Created);
            }
            catch(Exception e)
            {
                response = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }

            return response;
        }

        [HttpGet]
        public IEnumerable<Patient> GetAllPatients()
        {
            return patientRepository.GetPatients();
        }

        [HttpGet]
        public Patient GetPatient(int patientId)
        {
            Patient patient = patientRepository.GetPatientById(patientId);

            if(patient == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            return patient;
        }

        [HttpPut]
        public HttpResponseMessage UpdatePatientInformation(Patient patient)
        {
            HttpResponseMessage response;

            if(patient == null)
            {
                throw new ArgumentNullException("patient");
            }

            try
            {
                patientRepository.UpdatePatient(patient);
                patientRepository.Save();
            }
            catch(Exception e)
            {
                response = Request.CreateErrorResponse(HttpStatusCode.NotFound, e);
            }

            response = Request.CreateResponse(HttpStatusCode.OK);

            return response;
        }

        [HttpDelete]
        public HttpResponseMessage RemovePatient(int patientId)
        {
            HttpResponseMessage response;

            try
            {
                patientRepository.DeletePatient(patientId);
                patientRepository.Save();
                response = Request.CreateResponse(HttpStatusCode.OK);
            }
            catch(Exception e)
            {
                response = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }

            return response;
        }
    }
}