﻿using ClinicAppointments.BE.DAL;
using ClinicAppointments.BE.DAL.Interfaces;
using ClinicAppointments.BE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ClinicAppointments.BE.Controllers
{
    public class AppointmentController : ApiController
    {
        IAppointment appointmentRepository;

        public AppointmentController()
        {
            appointmentRepository = new AppointmentRepository();
        }

        [HttpPost]
        public HttpResponseMessage AddAppointmentInformation([FromBody]Appointment appointment)
        {
            HttpResponseMessage response;
            try
            {
                appointmentRepository.InsertAppointment(appointment);
                appointmentRepository.Save();
                response = Request.CreateResponse(HttpStatusCode.Created);
            }
            catch (Exception e)
            {
                response = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }

            return response;
        }

        [HttpGet]
        public IEnumerable<Appointment> GetAllAppointments()
        {
            return appointmentRepository.GetAppointments();
        }

        [HttpGet]
        public Appointment GetAppointment(int appointmentId)
        {
            Appointment appointment = appointmentRepository.GetAppointmentById(appointmentId);

            if (appointment == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            return appointment;
        }

        [HttpGet]
        public IEnumerable<Appointment> GetAppointmentsByPatient(int patientId)
        {
            return appointmentRepository.GetAppointmentsByPatient(patientId);
        }

        [HttpGet]
        public IEnumerable<Appointment> GetAppointmentsBySpeciality(int specialityId)
        {
            return appointmentRepository.GetAppointmentsBySpeciality(specialityId);
        }

        [HttpPut]
        public HttpResponseMessage UpdateAppointmentInformation(Appointment appointment)
        {
            HttpResponseMessage response;

            if (appointment == null)
            {
                throw new ArgumentNullException("appointment");
            }

            try
            {
                appointmentRepository.UpdateAppointment(appointment);
                appointmentRepository.Save();
            }
            catch (Exception e)
            {
                response = Request.CreateErrorResponse(HttpStatusCode.NotFound, e);
            }

            response = Request.CreateResponse(HttpStatusCode.OK);

            return response;
        }

        [HttpDelete]
        public HttpResponseMessage RemoveAppointment(int appointmentId)
        {
            HttpResponseMessage response;

            try
            {
                appointmentRepository.DeleteAppointment(appointmentId);
                appointmentRepository.Save();
                response = Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                response = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }

            return response;
        }
    }
}
