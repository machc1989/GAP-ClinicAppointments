﻿using ClinicAppointments.BE.DAL.Interfaces;
using ClinicAppointments.BE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClinicAppointments.BE.DAL
{
    public class SpecialityRepository : ISpeciality
    {
        ClinicContext context;

        public SpecialityRepository(ClinicContext context)
        {
            this.context = context;
        }

        public SpecialityRepository()
        {
            context = new ClinicContext();
        }

        IEnumerable<Speciality> ISpeciality.GetSpecialities()
        {
            return context.Specialities.ToList();
        }

        Speciality ISpeciality.GetSpecialityById(int specialityId)
        {
            return context.Specialities.Where(speciality => speciality.SpecialityId == specialityId).FirstOrDefault();
        }
    }
}