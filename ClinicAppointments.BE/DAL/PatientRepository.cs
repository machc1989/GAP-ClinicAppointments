﻿using ClinicAppointments.BE.DAL.Interfaces;
using ClinicAppointments.BE.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ClinicAppointments.BE.DAL
{
    public class PatientRepository : IPatient
    {
        private ClinicContext context;

        public PatientRepository(ClinicContext context)
        {
            this.context = context;
        }

        public PatientRepository()
        {
            context = new ClinicContext();
        }

        void IPatient.DeletePatient(int patientId)
        {
            Patient patient = context.Patients.Find(patientId);
            context.Patients.Remove(patient);
        }

        void IPatient.DeletePatient(Patient patient)
        {
            context.Patients.Remove(patient);
        }

        Patient IPatient.GetPatientById(int patientId)
        {
            return context.Patients.Find(patientId);
        }

        IEnumerable<Patient> IPatient.GetPatients()
        {
            return context.Patients.ToList();
        }

        void IPatient.InsertPatient(Patient patient)
        {
            context.Patients.Add(patient);
        }

        void IPatient.Save()
        {
            context.SaveChanges();
        }

        void IPatient.UpdatePatient(Patient patient)
        {
            context.Entry(patient).State = EntityState.Modified;
        }
    }
}