﻿using ClinicAppointments.BE.DAL.Interfaces;
using ClinicAppointments.BE.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ClinicAppointments.BE.DAL
{
    public class AppointmentRepository : IAppointment
    {
        ClinicContext context;

        public AppointmentRepository(ClinicContext context)
        {
            this.context = context;
        }

        public AppointmentRepository()
        {
            context = new ClinicContext();
        }

        public void DeleteAppointment(int appointmentId)
        {
            Appointment appointment = context.Appointments.Find(appointmentId);
            DeleteAppointment(appointment);
        }

        public void DeleteAppointment(Appointment appointment)
        {
            context.Appointments.Remove(appointment);
        }

        public Appointment GetAppointmentById(int appointmentId)
        {
            return context.Appointments.Find(appointmentId);
        }

        public IEnumerable<Appointment> GetAppointments()
        {
            return context.Appointments.ToList();
        }

        public IEnumerable<Appointment> GetAppointmentsByPatient(int patientId)
        {
            return context.Appointments.Where(appointment => appointment.PatientId == patientId).ToList();
        }

        public IEnumerable<Appointment> GetAppointmentsBySpeciality(int specialityId)
        {
            return context.Appointments.Where(appointment => appointment.SpecialityId == specialityId).ToList();
        }

        public void InsertAppointment(Appointment appointment)
        {
            context.Appointments.Add(appointment);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void UpdateAppointment(Appointment appointment)
        {
            context.Entry(appointment).State = EntityState.Modified;
        }
    }
}