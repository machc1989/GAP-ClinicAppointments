﻿using ClinicAppointments.BE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicAppointments.BE.DAL.Interfaces
{
    interface ISpeciality
    {
        IEnumerable<Speciality> GetSpecialities();
        Speciality GetSpecialityById(int specialityId);
    }
}
