﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClinicAppointments.BE.Models;

namespace ClinicAppointments.BE.DAL.Interfaces
{
    interface IAppointment
    {
        IEnumerable<Appointment> GetAppointments();
        IEnumerable<Appointment> GetAppointmentsBySpeciality(int specialityId);
        IEnumerable<Appointment> GetAppointmentsByPatient(int patientId);
        Appointment GetAppointmentById(int appointmentId);
        void InsertAppointment(Appointment appointment);
        void UpdateAppointment(Appointment appointment);
        void DeleteAppointment(int appointmentId);
        void DeleteAppointment(Appointment appointment);
        void Save();
    }
}
