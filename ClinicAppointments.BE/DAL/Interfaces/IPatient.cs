﻿using ClinicAppointments.BE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicAppointments.BE.DAL.Interfaces
{
    interface IPatient
    {
        IEnumerable<Patient> GetPatients();
        Patient GetPatientById(int patientId);
        void InsertPatient(Patient patient);
        void UpdatePatient(Patient patient);
        void DeletePatient(int patientId);
        void DeletePatient(Patient patient);
        void Save();
    }
}
