﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicAppointments.BW
{
    public class BusinessWorkFlow
    {
        public static bool TheAppointmentCanBeCanceled(DateTime appointmentDate)
        {
            bool result = true;
            int comparison = appointmentDate.CompareTo(DateTime.Now.AddDays(1));
            if(comparison < 0)
            {
                result = false;
            }

            return result;
        }

        public static bool ThePatientDontHaveAppointments(DateTime appointmentDate, IEnumerable<DateTime> listOfAppoinments)
        {
            bool result = true;
            int countOfAppointments = 
                listOfAppoinments.Where(
                    model => model.ToString("MM-dd-yyyy") == appointmentDate.ToString("MM-dd-yyyy")).Count();

            if(countOfAppointments > 0)
            {
                result = false;
            }

            return result;
        }
    }
}
