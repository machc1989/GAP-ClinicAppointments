﻿using ClinicAppointments.FE.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using System.Net.Http;

namespace ClinicAppointments.FE.Controllers
{
    [Authorize]
    public class AppointmentController : Controller
    {
        Appointment.Services services = new Appointment.Services();
        
        public ActionResult Index()
        {
            ViewBag.AvailableHours = GetListOfAvailableHours();
            ViewBag.FirstDay = GetFirstDayOfTheWeek(DateTime.Now);
            IEnumerable<Appointments> model = services.GetAppointments();

            return View(model);
        }

        public ActionResult AddAppointment(string date)
        {
            Appointments appointment = new Appointments();
            appointment.AppointmentDate = DateTime.ParseExact(date, "MM-dd-yyyy H:mm",
                                       System.Globalization.CultureInfo.InvariantCulture);
            return View("Create", appointment);
        }

        private Dictionary<int,string> GetListOfAvailableHours()
        {
            Dictionary<int, string> availableHours = new Dictionary<int, string>();
            availableHours.Add(1, "8:00");
            availableHours.Add(2, "8:30");
            availableHours.Add(3, "9:00");
            availableHours.Add(4, "9:30");
            availableHours.Add(5, "10:00");
            availableHours.Add(6, "10:30");
            availableHours.Add(7, "11:00");
            availableHours.Add(8, "11:30");
            availableHours.Add(9, "13:00");
            availableHours.Add(10, "13:30");
            availableHours.Add(11, "14:00");
            availableHours.Add(12, "14:30");
            availableHours.Add(13, "15:00");
            availableHours.Add(14, "15:30");
            availableHours.Add(15, "16:00");
            availableHours.Add(16, "16:30");

            return availableHours;
        }

        private DateTime GetFirstDayOfTheWeek(DateTime dayOfTheWeek)
        {
            DateTime firstDayOfTheWeek = dayOfTheWeek.AddDays(-(int)dayOfTheWeek.DayOfWeek);
            
            return firstDayOfTheWeek;
        }

        public ActionResult Update(int id)
        {
            Appointments appointment = services.GetAppointmentById(id);

            return View("Create", appointment);
        }

        public ActionResult AddOrEdit(Appointments appointment)
        {
            if(appointment.AppointmentId == 0)
            {
                IEnumerable<Appointments> listOfAppoinments = services.GetAppointmentByPatient(appointment.PatientId);
                IEnumerable<DateTime> listOfDatetime = (from list in listOfAppoinments select list.AppointmentDate).ToList();

                if (BW.BusinessWorkFlow.ThePatientDontHaveAppointments(appointment.AppointmentDate, listOfDatetime))
                {
                    services.AddAppointment(appointment);
                }
                else
                {
                    ViewBag.ErrorMessage = "The patient cannot have two appointments in the same day";
                    return View("Error");
                }
            }
            else
            {
                services.EditAppointment(appointment);
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            Appointments appointment = services.GetAppointmentById(id);

            return View(appointment);
        }

        [HttpPost]
        public ActionResult DeleteAppointment(Appointments appointment)
        {

            if (BW.BusinessWorkFlow.TheAppointmentCanBeCanceled(appointment.AppointmentDate))
            {
                services.DeleteAppointment(appointment.AppointmentId);
            }
            else
            {
                ViewBag.ErrorMessage = "The appointments only can be canceled 24 hours before";
                return View("Error");
            }

            return RedirectToAction("Index");
        }

        public ActionResult NextWeek(string date)
        {
            DateTime referenceDate = DateTime.ParseExact(date, "MM/dd/yyyy HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);
            ViewBag.AvailableHours = GetListOfAvailableHours();
            ViewBag.FirstDay = GetFirstDayOfTheWeek(referenceDate.AddDays(7));
            IEnumerable<Appointments> model = services.GetAppointments();

            return View("Index", model);
        }

        public ActionResult LastWeek(string date)
        {
            DateTime referenceDate = DateTime.ParseExact(date, "MM/dd/yyyy HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);
            ViewBag.AvailableHours = GetListOfAvailableHours();
            ViewBag.FirstDay = GetFirstDayOfTheWeek(referenceDate.AddDays(-7));
            IEnumerable<Appointments> model = services.GetAppointments();

            return View("Index", model);
        }
    }
}