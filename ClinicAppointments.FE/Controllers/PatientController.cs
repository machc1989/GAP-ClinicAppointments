﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ClinicAppointments.FE.Models;

namespace ClinicAppointments.FE.Controllers
{
    [Authorize]
    public class PatientController : Controller
    {
        Patient.Services services = new Patient.Services();

        public ActionResult Index()
        {
            IEnumerable<Patients> listOfPatients = new List<Patients>();
            try
            {
                listOfPatients = services.GetPatients();
            }
            catch
            {
                return View("Error");
            }
            return View(listOfPatients);
        }

        public ActionResult Create()
        {
            Patients patient = new Patients();
            return View("AddOrEdit", patient);
        }

        public ActionResult Update(int id)
        {
            Patients patient = services.GetPatientById(id);
            return View("AddOrEdit", patient);
        }

        [HttpPost]
        public ActionResult AddOrEdit(Patients patient)
        {
            try
            {
                if (patient.PatientId == 0)
                {
                    services.AddPatient(patient);
                }
                else
                {
                    services.EditPatient(patient);
                }
            }
            catch
            {
                return View("Error");
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            services.DeletePatient(id);
            return RedirectToAction("Index");
        }
    }
}