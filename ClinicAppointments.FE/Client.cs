﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Configuration;
using Newtonsoft.Json;
using System.Text;

namespace ClinicAppointments.FE
{
    public class Client<T> : IDisposable
    {
        HttpClient client;

        public IEnumerable<T> GetList(string urlService)
        {
            createClient();
            IEnumerable<T> listOfElements = new List<T>();
            HttpResponseMessage response = client.GetAsync(urlService).Result;
            if (response.IsSuccessStatusCode)
            {
                string result = response.Content.ReadAsStringAsync().Result;
                listOfElements = JsonConvert.DeserializeObject<IEnumerable<T>>(result);
            }

            return listOfElements;
        }

        public T GetData(string urlService)
        {
            createClient();
            T data;
            HttpResponseMessage response = client.GetAsync(urlService).Result;
            response.EnsureSuccessStatusCode();

            string result = response.Content.ReadAsStringAsync().Result;
            data = JsonConvert.DeserializeObject<T>(result);

            return data;
        }

        public void InsertData(string urlService, T data)
        {
            createClient();
            StringContent content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(urlService, content).Result;

            response.EnsureSuccessStatusCode();
        }

        public void UpdateData(string urlService, T data)
        {
            createClient();
            StringContent content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PutAsync(urlService, content).Result;

            response.EnsureSuccessStatusCode();
        }

        public void DeleteData(string urlService)
        {
            createClient();
            HttpResponseMessage response = client.DeleteAsync(urlService).Result;

            response.EnsureSuccessStatusCode();
        }

        private void createClient()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(ConfigurationManager.AppSettings["urlServer"]);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public void Dispose()
        {
            client.Dispose();
        }
    }
}