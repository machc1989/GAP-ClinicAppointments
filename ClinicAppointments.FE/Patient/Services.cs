﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ClinicAppointments.FE.Models;

namespace ClinicAppointments.FE.Patient
{
    public class Services
    {
        private Client<Patients> client;

        public Services()
        {
            client = new Client<Patients>();
        }
        
        public IEnumerable<Patients> GetPatients()
        {
            return client.GetList(Constants.GetAllPatients);
        }

        public Patients GetPatientById(int patientId)
        {
            string urlService = Constants.GetPatientById + patientId;
            return client.GetData(urlService);
        }

        public void AddPatient(Patients patient)
        {
            client.InsertData(Constants.AddPatient, patient);
        }

        public void EditPatient(Patients patient)
        {
            client.UpdateData(Constants.UpdatePatient, patient);
        }

        public void DeletePatient(int patientId)
        {
            string urlService = Constants.DeletePatient + patientId;
            client.DeleteData(urlService);
        }
    }
}