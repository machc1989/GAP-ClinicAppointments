﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClinicAppointments.FE.Models
{
    public class SpecialityModel
    {
    }

    public class Specialities
    {
        public int SpecialityId { get; set; }
        public string Name { get; set; }
    }
}