﻿using System.ComponentModel.DataAnnotations;

namespace ClinicAppointments.FE.Models
{
    public class PatientModels
    { }

    public class Patients
    {
        public int PatientId { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "LastName")]
        public string LastName { get; set; }
        [Display(Name = "Telephone")]
        public string Telephone { get; set; }
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Address")]
        public string Address { get; set; }
    }
}