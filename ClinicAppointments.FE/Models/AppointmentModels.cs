﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ClinicAppointments.FE.Models
{
    public class AppointmentModels
    {
    }

    public class Appointments
    {
        public int AppointmentId { get; set; }
        [Display(Name = "Patient")]
        public int PatientId { get; set; }
        [Display(Name = "Speciality")]
        public int SpecialityId { get; set; }
        [Display(Name = "Appointment Date")]
        public DateTime AppointmentDate { get; set; }

        public virtual Patients Patient { get; set; }
        public virtual Specialities Speciality { get; set; }
    }
}