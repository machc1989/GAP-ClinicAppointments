﻿using System.Web;
using System.Web.Mvc;

namespace ClinicAppointments.FE
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
