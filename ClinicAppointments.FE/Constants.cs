﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClinicAppointments.FE
{
    public class Constants
    {
        #region Patient
        private static string basePatientAPI = "api/Patient";
        public static string GetAllPatients = basePatientAPI;
        public static string GetPatientById = basePatientAPI + "?patientId=";
        public static string AddPatient = basePatientAPI;
        public static string UpdatePatient = basePatientAPI;
        public static string DeletePatient = GetPatientById;
        #endregion

        #region Appointment
        private static string baseAppointmentAPI = "api/Appointment";
        public static string GetAllAppointments = baseAppointmentAPI;
        public static string GetAppointmentById = baseAppointmentAPI + "?appointmentId=";
        public static string GetAppointmentByPatient = baseAppointmentAPI + "?patientId=";
        public static string GetAppointmentBySpeciality = baseAppointmentAPI + "?specialityId=";
        public static string AddAppointment = baseAppointmentAPI;
        public static string UpdateAppointment = baseAppointmentAPI;
        public static string DeleteAppointment = GetAppointmentById;
        #endregion

        #region Speciality
        private static string baseSpecialityAPI = "api/Speciality";
        public static string GetAllSpecialities = baseSpecialityAPI;
        #endregion
    }
}