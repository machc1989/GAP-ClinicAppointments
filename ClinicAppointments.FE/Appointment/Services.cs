﻿using ClinicAppointments.FE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ClinicAppointments.FE.Appointment
{
    public class Services
    {
        private Client<Appointments> client;

        public Services()
        {
            client = new Client<Appointments>();
        }

        public IEnumerable<Appointments> GetAppointments()
        {
            return client.GetList(Constants.GetAllAppointments);
        }

        public Appointments GetAppointmentById(int appointmentId)
        {
            string urlService = Constants.GetAppointmentById + appointmentId;
            return client.GetData(urlService);
        }

        public IEnumerable<Appointments> GetAppointmentByPatient(int patientId)
        {
            string urlService = Constants.GetAppointmentByPatient + patientId;
            return client.GetList(urlService);
        }

        public IEnumerable<Appointments> GetAppointmentBySpeciality(int specialityId)
        {
            string urlService = Constants.GetAppointmentBySpeciality + specialityId;
            return client.GetList(urlService);
        }

        public void AddAppointment(Appointments appointment)
        {
            client.InsertData(Constants.AddAppointment, appointment);
        }

        public void EditAppointment(Appointments appointment)
        {
            client.UpdateData(Constants.UpdateAppointment, appointment);
        }

        public void DeleteAppointment(int appointmentId)
        {
            string urlService = Constants.DeleteAppointment + appointmentId;
            client.DeleteData(urlService);
        }
    }
}