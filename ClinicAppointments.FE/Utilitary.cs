﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ClinicAppointments.FE.Models;

namespace ClinicAppointments.FE
{
    public static class Utilitary
    {
        public static IEnumerable<SelectListItem> GetAllPatients()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            Client<Patients> client = new Client<Patients>();

            IEnumerable<Patients> list = client.GetList(Constants.GetAllPatients);
            foreach(Patients patient in list)
            {
                SelectListItem item = 
                    new SelectListItem()
                        { Text = patient.Name + " " + patient.LastName,
                        Value = patient.PatientId.ToString() };
                result.Add(item);
            }

            return result;
        }

        public static IEnumerable<SelectListItem> GetAllSpecialities()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            Client<Specialities> client = new Client<Specialities>();

            IEnumerable<Specialities> list = client.GetList(Constants.GetAllSpecialities);
            foreach (Specialities speciality in list)
            {
                SelectListItem item =
                    new SelectListItem()
                    {
                        Text = speciality.Name ,
                        Value = speciality.SpecialityId.ToString()
                    };
                result.Add(item);
            }

            return result;
        }
    }
}